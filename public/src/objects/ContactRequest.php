<?php

class ContactRequest
{
    private $connection;
    private $table_name = "contact_requests";

    public $id;
    public $name;
    public $email;
    public $message;
    public $phone;
    public $date_requested;

    public function __construct($db) {
        $this->connection = $db;
    }

    public function sendEmail() {
        if (strlen($this->phone) == 10) {
            // quick and dirty formatting for phone number - look at plugins to format better
            $this->phone = sprintf("%s-%s-%s", substr($this->phone, 0, 3), substr($this->phone, 3, 3), substr($this->phone, 6));
        }
        $headers[] = "MIME-Version: 1.0";
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';
        $headers[] = 'X-Mailer: PHP/' . phpversion();
        $to = "guy-smiley@example.com";
        $subject = "Dealer Inspire Challenge contact request";
        $emailMessage = "<!DOCTYPE html><html><body>";
        $emailMessage .= "<p>You have a contact request from the Dealer Inspire Challenge website. Details are below:</p>";
        $emailMessage .= "<table border='1' align='center' cellpadding='0' cellspacing='0' width='600' style='border-collapse: collapse';";
        $emailMessage .= "<tr><th>Name</th><td style='padding:3px;'>$this->name</td></tr>";
        $emailMessage .= "<tr><th>Email</th><td style='padding:3px;'>$this->email</td></tr>";
        $emailMessage .= "<tr><th>Phone</th><td style='padding:3px;'>$this->phone</td></tr>";
        $emailMessage .= "<tr><th>Message</th><td style='padding:3px;'>$this->message</td></tr>";
        $emailMessage .= "</table></body></html>";

        return mail($to, $subject, $emailMessage, implode("\r\n", $headers));
    }

    public function addContactRequest() {
        $sql = "INSERT INTO {$this->table_name} 
            SET name = :name, 
            email = :email, 
            phone = :phone, 
            message = :message";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam('name', $this->name);
        $stmt->bindParam('email', $this->email);
        $stmt->bindParam('phone', $this->phone);
        $stmt->bindParam('message', $this->message);

        if($stmt->execute()) {
            return true;
        }
        //TODO: add logging functionality for errors
        // printf("Error: %s.\n", $stmt->error);
        return false;
    }
}

?>