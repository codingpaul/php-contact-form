CREATE DATABASE `dealer_inspire`;

CREATE TABLE `dealer_inspire`.`contact_requests` 
(   `id` INT NOT NULL AUTO_INCREMENT , 
    `name` VARCHAR(100) NOT NULL , 
    `email` VARCHAR(100) NOT NULL ,
    `message` TEXT NOT NULL , 
    `phone` VARCHAR(20) NOT NULL , 
    `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
    PRIMARY KEY (`id`)) ENGINE = InnoDB;