function validated() {
    let valid = true;
    if ($('#inputName').val() == "") {
        $('#inputName').addClass('highlight');
        valid = false;
    }
    if ($('#inputEmail').val() == "") {
        $('#inputEmail').addClass('highlight');
        valid = false;
    }    
    if ($('#inputMessage').val() == "") {
        $('#inputMessage').addClass('highlight');
        valid = false;
    }
    if (!valid) {
        $('#message-status-alert').html("<div class='alert alert-danger' role='alert'><p>Please fill in required fields</p></div>");
    }
    return valid;
}