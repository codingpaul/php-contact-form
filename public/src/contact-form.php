<?php
    include_once('../config/database.php');
    include_once('objects/ContactRequest.php');

    $database = new Database();
    $db = $database->connect();

    $contactRequest = new ContactRequest($db);

    // verify all required fields exist
    $error = false;
    isset($_POST['inputName']) ? $contactRequest->name = $_POST['inputName'] : $error = true;
    isset($_POST['inputEmail']) ? $contactRequest->email = trim($_POST['inputEmail']) : $error = true;
    isset($_POST['inputMessage']) ? $contactRequest->message = $_POST['inputMessage'] : $error = true;
    $contactRequest->phone = trim($_POST['inputPhone']) != "" ? $_POST['inputPhone'] : 'not given';

    if (!$error) {
        // first send email
        if ($contactRequest->sendEmail()) {
            // then update database
            if ($contactRequest->addContactRequest()) {
                echo "<div class='alert alert-success' role='alert'><p>Your message was sent successfully</p></div>"; 
                return;
            }
        } 
        echo "<div class='alert alert-danger' role='alert'><p>Error processing message, please try again.</p></div>"; 
        return;
    }

  
?>