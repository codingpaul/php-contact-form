<?php

use PHPUnit\Framework\TestCase;

require __DIR__."/../../public/src/objects/ContactRequest.php";

class ContactRequestTest extends TestCase
{
    public function testThatWeCanSetName()
    {
        $cr = new ContactRequest(null);
        $cr->name = "Bob Marley";
        $this->assertEquals($cr->name, "Bob Marley");
    }

    public function testThatWeCanSetEmail()
    {
        $cr = new ContactRequest(null);
        $cr->email = "test@test.com";
        $this->assertEquals($cr->email, "test@test.com");
    }

    public function testThatWeCanSetMessage()
    {
        $cr = new ContactRequest(null);
        $cr->message = "This is a test message.";
        $this->assertEquals($cr->message, "This is a test message.");
    }
    
    public function testThatWeCanSetPhone()
    {
        $cr = new ContactRequest(null);
        $cr->phone = "214-724-0609";
        $this->assertEquals($cr->phone, "214-724-0609");
    }
    // public function testCanSendEmail()
    // {
    //     $cr = new ContactRequest(null);
    //     $this->assertTrue($cr->sendEmail(), "Email could not be sent, check mail server is configured and active on this server.");
    // }

}